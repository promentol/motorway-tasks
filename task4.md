# Animal Booking service

## Databse tables

As the operations on data are involved in transaction, the persistence layer is choosen to be relational database, like PostgreSQL or MySQL.
The following tables are used - `animals`, `bookings` and `animal_operations`.
### `animals` table 

| name  | type  | description  |
|---|---|---|
| `id`  | `int64`  | the `id` of animal |
| `name` | `varchar` | the `name` of animal  |
| `status`  | `enum` | showing the current `status` of the animal, `free` or `away`  |

### `bookings` table

| name  | type  | description  |
|---|---|---|
| `id`  | `int64`  | the `id` of the booking |
| `animalId` | `int64` | the `id` of the animal  |
| `userId` | `int64` | the `id` of the user, who booked  |
| `createdAt` | `date` | the date, when the booking was made  |
| `start_date` | `date` | the date, when the animal will be taken  |
| `end_date` | `date` | the date , when the animal will be returned  |
| `status`  | `enum` | showing the current `status` of the booking, `booked`, `grabbed`, `away`, `finished`  |

### `animal_operations` table

| name  | type  | description  |
|---|---|---|
| `id`  | `int64`  | the `id` of the booking |
| `userId`  | `int64`  | the `id` of the user, who represent the employee, who triggered the event |
| `bookingId` | `int64` | the `id` of the animal  |
| `event_name` | `enum` | showing the event, `grabbed`, `returned` |
| `date`  | `date` | date, when the event was created  |

# Architecture
API is written as a monolithic application, which can be used stand alone, or be a microservice for big project. `JWT` is choosen as a method for authorisation, so no database calls to `users` table will be there, so it can work completely isolated.
All api requests should include `Content-Type: application/json` header, and all requests are returning again with `Content-Type: application/json` header.

# Api Methods

The following endpoints are there, 

 * `GET /animals?startDate=:startDate&endDate=:endDate` - lists all animals, which are available for specified date interval. Looks to `animals` table with joins `bookings` table
 * `POST /bookings` - books an animal, should include `animalId`, `startDate` and `endDate` fields in request body. Should validate the `animalId` is real id, and the animal is free for that date interval, and then insert a row in `booking table`, with status field to be equal `booked`
 * `POST /bookings/:id/cancel` - should mark the `status` field of the specified booking as `cancelled`. Should validate that the `user`'s `id` is equal to the user who created the booking.
 * `GET /bookings?startDate=:startDate&endDate=:endDate` - lists all bookings. `startDate` and `endDate` are optional filters.
 * `POST /bookings/:id/grabbed` - Should insert a new row in `animals_operations` table, with `event_name` to be equal `grabbed`. Should also update `animals` table with corresponding status
  * `POST /bookings/:id/returned` - Should insert a new row in `animals_operations` table, with `event_name` to be equal `returned`. Should also update `animals` table with the status `free`.

All queries, used for processing an api requests, should be wrapped inside SQL transactions.

# Task Queue
As SQL doesn't guaranntee the orders of concurent queries, to avoid double bookings, the `POST /booking` request should insert a task into the task queue, and listen when the task will completed. Task queue can have multiple rooms, e.g. one room for one animal. As a task queue - `redis` or `rabbitMQ` can be usen. It is recommended to use `kue` or `bull` packages from `npm`. It is possible, that while validating, if the animal free for those dates, it will be already booked, as the previous entries in the queue have already booked the animal, so the api should return error in that case. 

# Alternative consideration
We can get rid of from `animal_operations` table, and instead use new columns in `bookings` table - `grab_date` and `return_date`. The advantage of `animals_table` is, that for certain booking, it can support multiple operations ( multiple grabs and returns, as well new types of operations, like `vaccinated` or `damaged` or so)

### Todos

 - Add Swagger, Apiary other api documentation
