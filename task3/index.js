const axios = require('axios'),
    _ = require('lodash'),
    API_URL = 'https://motorway-challenge-api.herokuapp.com/api';

(async () => {

    var token = await fetchToken(),
        visits = await fetchRawData(token),
        filteredVisits = filterVisits(visits),
        groupedVisits = _(filteredVisits).groupBy('name').mapValues((x)=>x.length).value();


    console.log(groupedVisits)

    return groupedVisits;

})().catch(e=>console.log(e))


async function fetchToken() {
    const {
        data: {
            token
        }
    } = await axios.get(`${API_URL}/login`)

    return token
}

async function fetchRawData(token) {

    const {
        data: firstPageData,
        total: allItemsCount
    } = await fetchPage(1, token);

    // adding 1 more page, for safety, as a new data may be added
    // we are safe to add one page, because the script will run in a short time, and visitors may not be added too much in that short time 
    const pageCount = Math.ceil(allItemsCount/firstPageData.length)+1;

    const rawData = await Promise.all(
        generateInterval(pageCount).map(async (page) => {
            const {
                data
            } = await fetchPage(page, token);

            return data;
        })
    )
    
    const finalData = _(rawData).flatten().uniqBy('id');
    return finalData
}

async function fetchPage(pageNumber, token) {
    const {
        data: {
            data,
            total
        }
    } = await axios.get(`${API_URL}/visits?page=${pageNumber}&token=${token}`);

    return {
        data, total
    }
}

function generateInterval(N) {
    return [...Array(N).keys()].map((x)=>x+1)
}

function filterVisits(visits) {
    return visits
        .filter((visit) => !isToday(visit))
        .filter((visit) => !isWeekend(visit))
}

function isToday (dateString) {
    const today = new Date(), date = new Date(dateString);
    return date.getDate() == today.getDate() &&
      date.getMonth() == today.getMonth() &&
      date.getFullYear() == today.getFullYear();
}
  
function isWeekend (dateString) {
    const date = new Date(dateString);
    //return true, if day is 0 or 6 
    return date.getDay()%6 == 0
}