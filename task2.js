var val1 = 65;
var val2 = 8921;
var factor1 = 16807;
var factor2 = 48271;
var count = 0;

for(var i =0; i<1000000; ++i) {

    // (val1-val2) % 65536 == 0
    if( (val1 & 65535) == (val2 & 65535) ) {
        console.log(i);
        ++count;
    }
    val1 = val1 * factor1 % 2147483647;
    val2 = val2 * factor2 % 2147483647;
}

console.log("The Count is:" , count);