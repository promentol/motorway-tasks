function encodeRLE(arrayOfCharacters) {
    var answer = "", count = 0;
    for(var i in arrayOfCharacters) {
        if(i > 0 && arrayOfCharacters[i] != arrayOfCharacters[i-1]) {
            answer+=count+arrayOfCharacters[i-1]
            count = 0;
        }
        ++count;
    }

    answer+=count+arrayOfCharacters[arrayOfCharacters.length-1];

    return answer;
}


console.log(encodeRLE(['m', 'm', 'm', 'o', 'o', 'o', 't', 't', 'o', 'o', 'r', 'r', 'r', 'r', 'r', 'w', 'w', 'a', 'y', 'y', 'y', 'y']))